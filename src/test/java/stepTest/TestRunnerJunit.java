package stepTest;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature/demo.feature"
,glue="stepTest",plugin = { "pretty", "html:target/cucumber-re" })

public class TestRunnerJunit {

}
