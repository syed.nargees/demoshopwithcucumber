package com.demopageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Giftcard {
	WebDriver driver;//instance variable

	public Giftcard(WebDriver driver)//local variable
	{
		this.driver=driver;//this.driver is instance variable
	}
	@FindBy(partialLinkText="Gift Cards")
	private WebElement clickgiftcards;
	public void clickGiftCard()
	{
		clickgiftcards.click();
	}
	
	@FindBy(id="products-pagesize")
	private WebElement selectgiftcard;
	public void selectGiftCard()
	{
		Select sl = new Select(selectgiftcard);
		sl.selectByVisibleText("4");
	}
	
	@FindBy(xpath="//a[text()='$5 Virtual Gift Card']")
	private WebElement displaycartdetails;
	
	public void displayGiftCart()
	{
		System.out.println(displaycartdetails.getText());
	}

}
