package com.demopageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Electronics {
	WebDriver driver;//instance variable

	public Electronics(WebDriver driver)//local variable
	{
		this.driver=driver;//this.driver is instance variable
	}
	@FindBy(xpath="(//a[@href=\"/electronics\"])[1]")
	private WebElement electronicsbtn;
	public void clickelectronics() {
		//electronicsbtn.click();
		Actions a=new Actions(driver);
		a.moveToElement(electronicsbtn).build().perform();
	}
	@FindBy(xpath="(//a[@href=\"/cell-phones\"])[1]")
	private WebElement cellphone;
	public void cellphone() {
		//electronicsbtn.click();
		Actions a=new Actions(driver);
		a.doubleClick(cellphone).perform();
	}
	@FindBy(xpath ="(//input[@value=\"Add to cart\"])[1]")
	private WebElement addtobtn1;
	public void addToCart1()
	{
		addtobtn1.click();
	}
	@FindBy(xpath="//span[@class='cart-qty']")
	private WebElement shoping;
	public void shopingCart1() throws InterruptedException
	{Thread.sleep(3000);
		String text = shoping.getText();
		System.out.println(text);
	}
}
