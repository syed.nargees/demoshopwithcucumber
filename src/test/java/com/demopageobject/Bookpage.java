package com.demopageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Bookpage {
	WebDriver driver;//instance variable

	public Bookpage(WebDriver driver)//local variable
	{
		this.driver=driver;//this.driver is instance variable
	}
	@FindBy(xpath="(//a[@href=\"/books\"])[1]")
	private WebElement booksbtn;
	public void clickbooks() {
		booksbtn.click();
	}
@FindBy(id="products-orderby")
private WebElement sortby;
public void sort()
{
	Select s=new Select(sortby);
	s.selectByVisibleText("Price: High to Low");
}
@FindBy(xpath ="(//input[@value=\"Add to cart\"])[1]")
private WebElement addtobtn1;
public void addToCart1()
{
	addtobtn1.click();
}
@FindBy(xpath ="(//input[@value='Add to cart'])[2]")
private WebElement addtobtn2;
public void addToCart2()
{
	addtobtn2.click();
}
}
