package com.demopageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Logout {
	WebDriver driver;//instance variable

	public Logout(WebDriver driver)//local variable
	{
		this.driver=driver;//this.driver is instance variable
	}
	@FindBy(linkText="Log out")
	private WebElement clicklogout;
	
	public void clickLogout()
	{
		clicklogout.click();
	}
}
