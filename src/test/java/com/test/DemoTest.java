package com.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.demopageobject.Bookpage;
import com.demopageobject.Electronics;
import com.demopageobject.Giftcard;
import com.demopageobject.Loginpage;
import com.demopageobject.Logout;

public class DemoTest {
	
	WebDriver driver=null;
	@BeforeClass
	private void setup() {
		driver=new ChromeDriver();
		
		
	}
	@Test(priority=1)
	private void sigin() {
		
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		//Login_pagewith_pageFactory login=PageFactory.initElements(driver,Login_pagewith_pageFactory.class;)
		Loginpage lp=PageFactory.initElements(driver,Loginpage.class);
		lp.getFirstlogin();
		
		lp.getEmail("nargees599@gmail.com");
		lp.getPassword("987654321$");
		lp.getLogin();
		
	}
	@Test(priority=2)
	public void book() throws InterruptedException
	{
		Bookpage bp=PageFactory.initElements(driver,Bookpage.class);
		Thread.sleep(3000);
		bp.clickbooks();
		bp.sort();
		bp.addToCart1();
		Thread.sleep(3000);
		bp.addToCart2();
		
	}
	@Test(priority=3)
	public void electonics() throws InterruptedException
	{
		Electronics ep=PageFactory.initElements(driver,Electronics.class);
		Thread.sleep(3000);
		ep.clickelectronics();
		ep.cellphone();
		ep.addToCart1();
		ep.shopingCart1();
		
	}
	@Test(priority=4)
	public void giftcard() throws InterruptedException
	{
		Giftcard gp=PageFactory.initElements(driver,Giftcard.class);
		Thread.sleep(3000);
		gp.clickGiftCard();
		gp.selectGiftCard();
		gp.displayGiftCart();
	}
	@Test(priority=5)
	public void logout() throws InterruptedException
	{
		Logout log=PageFactory.initElements(driver,Logout.class);
		Thread.sleep(3000);
		log.clickLogout();
		if (driver.findElement(By.linkText("Log in")).isDisplayed()) {
            System.out.println("login is displayed");
        } else {
            System.out.println("login is not displayed");
        }
	}
}
