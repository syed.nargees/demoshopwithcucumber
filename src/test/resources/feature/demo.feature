Feature: TricentsDemoWeb shop Application to validate
Scenario: To check the login functionality and shopping features
Given user have to enter DemoWebShop application through chrome browser
When click on the login button
And user have to enter valid "<email>" and valid "<password>"
Then user have to click login button
And user have to get title of the home page
Examples:
|         email       | password |
|nargees123@gmail.com| 987654321$|
Scenario: To validate books functionality
Given user have to click book category
When user have to  sort price high to low
Then user have to Add books in add to cart

Scenario: To validate electonics functionality
Given user have to click Electronic category
When user have to select cellphone
Then user have to add any product in add to cart
And user have to display count of items present in shopping cart

Scenario: To validate Giftcard  functionality
Given user have to click on  gift card category
When user have to select the four  giftcards perpage
When user have to capture any one of the giftcard name and price

Scenario: To validate logout  functionality
Given user have to logout the application
Then login button should be displayed  on the homepage
